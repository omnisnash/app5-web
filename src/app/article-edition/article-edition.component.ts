import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Article } from '../article/article.component'
import { ArticleService } from '../services/article.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-article-edition',
  templateUrl: './article-edition.component.html',
  styleUrls: ['./article-edition.component.css']
})
export class ArticleEditionComponent implements OnInit {

  articleForm : FormGroup;
  editedArticle: Article;
  status: string;
  statusType: string;

  constructor(private fb: FormBuilder, private articleService: ArticleService, private route: ActivatedRoute, private router: Router) { }

  updateArticle(): void {
    let articleInfor: Article = this.articleForm.value;
    this.editedArticle.authors = articleInfor.authors;
    this.editedArticle.title = articleInfor.title;
    this.editedArticle.content = articleInfor.content;

    this.articleService.updateArticle(this.editedArticle).subscribe(
      (data) => {
        this.status = 'Article modifié !';
        this.statusType = 'green';
      },
      (error) => {
        this.status = 'Echec lors de la modification: ' + error;
        this.statusType = 'red';
      }
    )
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.articleService.getArticle(id).subscribe(
      (data) => {
        this.editedArticle = data;
        this.articleForm = this.fb.group({
          title: [data.title, Validators.required ],
          content : [data.content, Validators.required ],
          authors : [data.authors, Validators.required ],
        });
      },
      (error) => this.router.navigate(['/404'])
    );      
  }

}
