import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../services/article.service';


@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input("article") article : Article;
  @Output() deletedArticle : EventEmitter<Article> = new EventEmitter();

  detailMode: boolean = false; // in list view = false
  status: string;
  statusType: string;

  constructor(private route: ActivatedRoute, private articleService: ArticleService, private router: Router){
    this.status = "";
    this.statusType = "";
  }

  delete(){
    this.articleService.removeArticle(this.article.id).subscribe(
      data => {
        this.status = 'Article supprimé avec succès !';
        this.statusType = 'green'; // c pa bo
        this.deletedArticle.emit(this.article);
      },
      error => {
        this.status = 'Erreur lors de la suppression :(';
        this.statusType = 'red';
      }
    );
  }

  ngOnInit() {
    if(this.article === undefined) {
      this.detailMode = true;
      
      const id = this.route.snapshot.paramMap.get('id');
      this.articleService.getArticle(id).subscribe(
        (data) => this.article = data,
        (error) => this.router.navigate(['/404'])
      );
    }
  }
}

export interface Article {
  id: string;
  title: string;
  authors: string;
  content: string;
}
