import { Injectable } from '@angular/core';
import { Article } from '../article/article.component'
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, Subject } from "rxjs";

@Injectable()
export class ArticleService {

  constructor(private http: HttpClient) {
  }

  public getArticles() : Observable<Article[]> {
    return this.http.get<Article[]>("http://localhost:3000/articles")
  } 

  public getArticle(id: String) : Observable<Article> {
    return this.http.get<Article>("http://localhost:3000/articles/" + id)
  } 

  public removeArticle(id: string) {
    return this.http.delete<Article>("http://localhost:3000/articles/" + id);
  }

  public addArticle(article: Article) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Article>('http://localhost:3000/articles', article, {'headers' : headers});
  }

  public updateArticle(article: Article) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.put<Article>('http://localhost:3000/articles/' + article.id, article, {'headers' : headers});
  }
}
