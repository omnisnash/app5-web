import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Article } from '../article/article.component'
import { ArticleService } from '../services/article.service';

@Component({
  selector: 'app-article-creation',
  templateUrl: './article-creation.component.html',
  styleUrls: ['./article-creation.component.css']
})
export class ArticleCreationComponent implements OnInit {
 
  articleForm : FormGroup;
  status: string;
  statusType: string;

  constructor(private fb: FormBuilder, private articleService: ArticleService) {
    this.articleForm = this.fb.group({
      title: ['Fake Title', Validators.required ],
      content : ['', Validators.required ],
      authors : ['', Validators.required ],
    });
  }

  createArticle(): void {
    let article: Article = this.articleForm.value;

    this.articleService.addArticle(article).subscribe(
      (data) => {
        this.status = 'Article ajouté ! Vous pouvez en créer un autre';
        this.statusType = 'green';
        this.articleForm.reset();
      },
      (error) => {
        this.status = 'Echec lors de la création: ' + error;
        this.statusType = 'red';
      }
    )
  }

  ngOnInit() {
  }

}
