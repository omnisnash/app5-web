import { Component, OnInit } from '@angular/core';
import { Article } from '../article/article.component';
import { ArticleService } from '../services/article.service';
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  _articles: Article[];
  displayedArticles: Article[]; // filtered article
  articleSearchFilter: ArticleSearch; // provides the article filter feature

  constructor(private articleService: ArticleService) {
    this._articles = new Array();
    this.displayedArticles = new Array();
    this.articleSearchFilter = new ArticleSearch();
  }

  articles(): Article[] {
    return this._articles;
  }

  // Remove event from ArticleComponent
  onRemove(article: Article) {
    let index = this._articles.indexOf(article);
    if (index > -1) {
        this._articles.splice(index, 1);
        this.onFilterChange();
    }
  }

  // Filter parameter change
  // Todo: add debounce feature
  public onFilterChange() {
    this.displayedArticles = this.articleSearchFilter.filter(this._articles);
  }

  ngOnInit() {
    this.articleService.getArticles().subscribe(
      (data) => {
        this._articles = data;
        this.onFilterChange();
      }
    );
  }

}

class ArticleSearch {
  titleContains: string;
  contentContains: string;
  authorsContains: string;

  constructor() {
    this.titleContains = "";
    this.contentContains = "";
    this.authorsContains = "";
  }

  public filter(articles: Article[]) : Article[] {
    let match = new Array<Article>();
    
    // Todo: use array.filter;
    for(let article of articles) {
      let pass: boolean = true;

      if(pass && this.titleContains) {
        pass = article.title.toLowerCase().trim().includes(this.titleContains.toLowerCase());
      }

      if(pass && this.contentContains) {
        pass = article.content.toLowerCase().trim().includes(this.contentContains.toLowerCase());
      }

      if(pass && this.authorsContains) {
        pass = article.authors.toLowerCase().trim().includes(this.authorsContains.toLowerCase());
      }

      if(pass) {
        match.push(article);
      }
    }

    return match;
  }

}
