# Algorithmes pour le Web

* Polytech Paris-Sud / APP5-INFO
* Thomas Cottin, Clément Garin
* 2017 - 2018

# Installation
Cloner le projet puis :

`npm install`

# Utilisation

Le projet n'utilise pas de sources de données réelles, uniquement du mock.

Lancement de la base de données JSON Server, à la racine du projet :

`json-server --watch db.json`

Puis lancement du serveur web de developpement:

`ng serv`

# Notes

Le projet utilise le [W3.CSS](https://www.w3schools.com/w3css/). Il n'est pas présent localement (voir `src/index.html`).

